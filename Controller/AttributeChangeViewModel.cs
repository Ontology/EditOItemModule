﻿using EditOItemModule.Models;
using EditOItemModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditOItemModule.Controller
{
    public class AttributeChangeViewModel: ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool? boolnull_Val;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.CheckBox, ViewItemId = "inpValue", ViewItemType = ViewItemType.Content)]
        public bool? BoolNull_Val
        {
            get { return boolnull_Val; }
            set
            {
                if (boolnull_Val == value) return;

                boolnull_Val = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_BoolNull_Val);

            }
        }

        private long? longnull_Val;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "inpValue", ViewItemType = ViewItemType.Content)]
        public long? LongNull_Val
        {
            get { return longnull_Val; }
            set
            {
                if (longnull_Val == value) return;

                longnull_Val = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_LongNull_Val);

            }
        }

        private double? doublenull_Val;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "inpValue", ViewItemType = ViewItemType.Content)]
        public double? DoubleNull_Val
        {
            get { return doublenull_Val; }
            set
            {
                if (doublenull_Val == value) return;

                doublenull_Val = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DoubleNull_Val);

            }
        }

        private DateTime? datetimenull_Val;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.DateTimeInput, ViewItemId = "inpValue", ViewItemType = ViewItemType.Content)]
        public DateTime? DateTimeNull_Val
        {
            get { return datetimenull_Val; }
            set
            {
                if (datetimenull_Val == value) return;

                datetimenull_Val = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DateTimeNull_Val);

            }
        }

        private string text_Val;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TextArea, ViewItemId = "inpValue", ViewItemType = ViewItemType.Content)]
        public string Text_Val
        {
            get { return text_Val; }
            set
            {
                if (text_Val == value) return;

                text_Val = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Val);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private bool isenabled_ValCheckbox;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.CheckBox, ViewItemId = "inpValue", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ValCheckbox
        {
            get { return isenabled_ValCheckbox; }
            set
            {
                if (isenabled_ValCheckbox == value) return;

                isenabled_ValCheckbox = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ValCheckbox);

            }
        }

        private bool isenabled_ValTextarea;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TextArea, ViewItemId = "inpValue", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ValTextarea
        {
            get { return isenabled_ValTextarea; }
            set
            {
                if (isenabled_ValTextarea == value) return;

                isenabled_ValTextarea = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ValTextarea);

            }
        }

        private bool isenabled_ValNumeric;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "inpValue", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ValNumeric
        {
            get { return isenabled_ValNumeric; }
            set
            {
                if (isenabled_ValNumeric == value) return;

                isenabled_ValNumeric = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ValNumeric);

            }
        }

        private string label_ItemIdentification;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ItemIdentification, ViewItemId = "@classItem@", ViewItemType = ViewItemType.Content)]
        public string Label_ItemIdentification
        {
            get { return label_ItemIdentification; }
            set
            {
                if (label_ItemIdentification == value) return;

                label_ItemIdentification = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ItemIdentification);

            }
        }

        private string label_ItemIdentificationLabel;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "itemIdentificationLabel", ViewItemType = ViewItemType.Content)]
        public string Label_ItemIdentificationLabel
        {
            get { return label_ItemIdentificationLabel; }
            set
            {
                if (label_ItemIdentificationLabel == value) return;

                label_ItemIdentificationLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ItemIdentificationLabel);

            }
        }

        private string label_AttributeTypeCheckbox;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.CheckBox, ViewItemId = "inpValue", ViewItemType = ViewItemType.Caption)]
        public string Label_AttributeTypeCheckbox
        {
            get { return label_AttributeTypeCheckbox; }
            set
            {
                if (label_AttributeTypeCheckbox == value) return;

                label_AttributeTypeCheckbox = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_AttributeTypeCheckbox);

            }
        }

        private string label_AttributeType;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "attributeTypeLabel", ViewItemType = ViewItemType.Content)]
        public string Label_AttributeType
        {
            get { return label_AttributeType; }
            set
            {
                if (label_AttributeType == value) return;

                label_AttributeType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_AttributeType);

            }
        }

        private bool isenabled_DateTimePicker;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.DateTimeInput, ViewItemId = "inpValue", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_DateTimePicker
        {
            get { return isenabled_DateTimePicker; }
            set
            {
                if (isenabled_DateTimePicker == value) return;

                isenabled_DateTimePicker = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_DateTimePicker);

            }
        }

        private AttributeItem attributeitem_NewAttribute;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "newAttribute", ViewItemType = ViewItemType.Other)]
        public AttributeItem AttributeItem_NewAttribute
        {
            get { return attributeitem_NewAttribute; }
            set
            {
                if (attributeitem_NewAttribute == value) return;

                attributeitem_NewAttribute = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_AttributeItem_NewAttribute);

            }
        }

        private string text_NewGuid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "newGuid", ViewItemType = ViewItemType.Other)]
        public string Text_NewGuid
        {
            get { return text_NewGuid; }
            set
            {
                if (text_NewGuid == value) return;

                text_NewGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_NewGuid);

            }
        }

        private string label_Save;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveAttribute", ViewItemType = ViewItemType.Content)]
		public string Label_Save
        {
            get { return label_Save; }
            set
            {
                if (label_Save == value) return;

                label_Save = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Save);

            }
        }

        private bool isenabled_Save;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveAttribute", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Save
        {
            get { return isenabled_Save; }
            set
            {

                isenabled_Save = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Save);

            }
        }
    }
}
