﻿using EditOItemModule.Models;
using EditOItemModule.Services;
using EditOItemModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace EditOItemModule.Controller
{
    public class AttributeChangeController : AttributeChangeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ElasticServiceAgent elasticServiceAgent;

        private clsTransaction transactionManager;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        private clsRelationConfig relationConfig;

        private string viewIdLongAttribute = "c8df2d290506490489e6423caef95808";
        private string viewIdBoolAttribute = "b6c235c44a5a42a2b5f6e6ae75dbf753";
        private string viewIdDateTimeAttribute = "27e56a1b78e84394863ea5738b6cacb8";
        private string viewIdDoubleAttribute = "c31480ee106c42d78a9fab3d132d4697";
        private string viewIdStringAttribute = "0836057505884a76a922b3c34dbb7e52";
        private string viewIdNewAttribute = "459d18a3fd794cdeadc6c05ab3ce1bec";

        private clsObjectAtt objectAttribute;
        private clsOntologyItem relatedObject;
        private clsOntologyItem relatedAttributeType;
        private clsOntologyItem relatedClass;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public AttributeChangeController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            PropertyChanged += AttributeChangeController_PropertyChanged;
        }

        private void CompleteNewAttributeItem(AttributeItem requestItem)
        {
            clsOntologyItem objectItem = null;
            clsOntologyItem classItem = null;
            clsOntologyItem attributeTypeItem = null;

            if (!localConfig.Globals.is_GUID(requestItem.IdClass) ||
                !localConfig.Globals.is_GUID(requestItem.IdObject) ||
                !localConfig.Globals.is_GUID(requestItem.IdAttributeType))
            {
                requestItem = null;
                AttributeItem_NewAttribute = requestItem;
                return;
            }
            if (string.IsNullOrEmpty(requestItem.NameClass))
            {
                classItem = elasticServiceAgent.GetOItem(requestItem.IdClass, localConfig.Globals.Type_Class);
                if (classItem == null)
                {
                    requestItem = null;
                    AttributeItem_NewAttribute = requestItem;
                    return;
                }

                requestItem.NameClass = classItem.Name;
            }

            if (string.IsNullOrEmpty(requestItem.NameObject))
            {
                objectItem = elasticServiceAgent.GetOItem(requestItem.IdObject, localConfig.Globals.Type_Object);
                if (objectItem == null)
                {
                    requestItem = null;
                    AttributeItem_NewAttribute = requestItem;
                    return;
                }

                requestItem.NameObject = objectItem.Name;
            }

            if (string.IsNullOrEmpty(requestItem.NameAttributeType) ||
                !localConfig.Globals.is_GUID(requestItem.IdDataType))
            {
                attributeTypeItem = elasticServiceAgent.GetOItem(requestItem.IdAttributeType, localConfig.Globals.Type_AttributeType);
                if (attributeTypeItem == null)
                {
                    requestItem = null;
                    AttributeItem_NewAttribute = requestItem;
                    return;
                }

                requestItem.NameAttributeType = attributeTypeItem.Name;

                requestItem.IdDataType = attributeTypeItem.GUID_Parent;

                if (requestItem.IdDataType == localConfig.Globals.DType_Bool.GUID)
                {
                    requestItem.NameDataType = localConfig.Globals.DType_Bool.Name;
                }
                else if (requestItem.IdDataType == localConfig.Globals.DType_DateTime.GUID)
                {
                    requestItem.NameDataType = localConfig.Globals.DType_DateTime.Name;
                }
                else if (requestItem.IdDataType == localConfig.Globals.DType_Int.GUID)
                {
                    requestItem.NameDataType = localConfig.Globals.DType_Int.Name;
                }
                else if (requestItem.IdDataType == localConfig.Globals.DType_Real.GUID)
                {
                    requestItem.NameDataType = localConfig.Globals.DType_Real.Name;
                }
                else if (requestItem.IdDataType == localConfig.Globals.DType_String.GUID)
                {
                    requestItem.NameDataType = localConfig.Globals.DType_String.Name;
                }
            }

            if (string.IsNullOrEmpty(requestItem.IdAttribute))
            {
                requestItem.IdAttribute = localConfig.Globals.NewGUID;
            }

            AttributeItem_NewAttribute = requestItem;
        }

        private void AttributeChangeController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));


            webSocketServiceAgent.SendPropertyChange(e.PropertyName);


            if (objectAttribute != null && (e.PropertyName == Notifications.NotifyChanges.ViewModel_BoolNull_Val ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DoubleNull_Val ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_LongNull_Val ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DateTimeNull_Val ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Text_Val))
            {
                var interSendAttChange = false;
                clsObjectAtt objectAttSend = null;
                objectAttSend = objectAttribute.Clone();
                if (e.PropertyName == Notifications.NotifyChanges.ViewModel_BoolNull_Val)
                {
                    if (objectAttribute != null && objectAttribute.Val_Bool != BoolNull_Val)
                    {
                        objectAttSend.Val_Bool = BoolNull_Val;
                        interSendAttChange = true;
                    }
                    else if (objectAttribute == null
                        && webSocketServiceAgent.IdEntryView == viewIdBoolAttribute
                        && objectAttribute.Val_Bool != BoolNull_Val
                        && relatedObject != null
                        && relatedAttributeType != null)
                    {
                        objectAttSend = relationConfig.Rel_ObjectAttribute(relatedObject, relatedAttributeType, BoolNull_Val);
                        interSendAttChange = true;
                    }
                }
                else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_LongNull_Val)
                {
                    if (objectAttribute != null && objectAttribute.Val_Int != LongNull_Val)
                    {
                        objectAttSend.Val_Int = LongNull_Val;
                        interSendAttChange = true;
                    }
                    else if (objectAttribute == null
                        && webSocketServiceAgent.IdEntryView == viewIdBoolAttribute
                        && objectAttribute.Val_Int != LongNull_Val
                        && relatedObject != null
                        && relatedAttributeType != null)
                    {
                        objectAttSend = relationConfig.Rel_ObjectAttribute(relatedObject, relatedAttributeType, LongNull_Val);
                        interSendAttChange = true;
                    }
                }
                else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_DoubleNull_Val)
                {
                    if (objectAttribute != null && objectAttribute.Val_Double != DoubleNull_Val)
                    {
                        objectAttSend.Val_Double = DoubleNull_Val;
                        interSendAttChange = true;
                    }
                    else if (objectAttribute == null
                        && webSocketServiceAgent.IdEntryView == viewIdDoubleAttribute
                        && objectAttribute.Val_Double != DoubleNull_Val
                        && relatedObject != null
                        && relatedAttributeType != null)
                    {
                        objectAttSend = relationConfig.Rel_ObjectAttribute(relatedObject, relatedAttributeType, DoubleNull_Val);
                        interSendAttChange = true;
                    }
                }
                else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_DateTimeNull_Val)
                {
                    if (objectAttribute != null && objectAttribute.Val_Datetime != DateTimeNull_Val)
                    {
                        objectAttSend.Val_Datetime = DateTimeNull_Val;
                        interSendAttChange = true;
                    }
                    else if (objectAttribute == null
                        && webSocketServiceAgent.IdEntryView == viewIdDateTimeAttribute
                        && objectAttribute.Val_Datetime != DateTimeNull_Val
                        && relatedObject != null
                        && relatedAttributeType != null)
                    {
                        objectAttSend = relationConfig.Rel_ObjectAttribute(relatedObject, relatedAttributeType, DateTimeNull_Val);
                        interSendAttChange = true;
                    }
                }
                else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Text_Val)
                {
                    if (objectAttribute != null && objectAttribute.Val_String != Text_Val)
                    {
                        objectAttSend.Val_String = Text_Val;
                        interSendAttChange = true;
                    }
                    else if (objectAttribute == null
                        && webSocketServiceAgent.IdEntryView == viewIdStringAttribute
                        && objectAttribute.Val_String != Text_Val
                        && relatedObject != null
                        && relatedAttributeType != null)
                    {
                        objectAttSend = relationConfig.Rel_ObjectAttribute(relatedObject, relatedAttributeType, Text_Val);
                        interSendAttChange = true;
                    }
                }

                if (interSendAttChange)
                {
                    var changeNotification = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.ChannelId_ChangedAttributeVal,
                        SenderId = webSocketServiceAgent.EndpointId,
                        GenericParameterItems = new List<object>
                        {
                            objectAttSend
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(changeNotification);
                }
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_AttributeItem_NewAttribute)
            {
                if (AttributeItem_NewAttribute != null)
                {
                    IsEnabled_Save = true;
                }
                else
                {
                    IsEnabled_Save = false;

                }
            }
            
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            relationConfig = new clsRelationConfig(localConfig.Globals);

            transactionManager = new clsTransaction(localConfig.Globals);

            elasticServiceAgent = new ElasticServiceAgent(localConfig);
            elasticServiceAgent.PropertyChanged += ElasticServiceAgent_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            elasticServiceAgent.StopRead();
            webSocketServiceAgent.RemoveAllResources();
            elasticServiceAgent = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
           
            IsSuccessful_Login = true;
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AddAttribute,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            var parameterRequest = new InterServiceMessage
            {
                ChannelId = Channels.ParameterList,
                SenderId = webSocketServiceAgent.EndpointId
            };
            webSocketServiceAgent.SendInterModMessage(parameterRequest);
            
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                webSocketServiceAgent.SetVisibility(true);
                webSocketServiceAgent.SetEnable(false);
                webSocketServiceAgent.SendModel();

                Label_ItemIdentificationLabel = translationController.Label_ItemIdentification;

                IsEnabled_Save = false;
                Label_Save = translationController.Label_Save;
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void ElasticServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            

        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {

                Initialize();
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "SetNewGuid")
                {
                    AttributeItem_NewAttribute.IdAttribute = localConfig.Globals.NewGUID;

                    Text_NewGuid = AttributeItem_NewAttribute.IdAttribute;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SaveItem" && AttributeItem_NewAttribute != null)
                {
                    
                    transactionManager.ClearItems();

                    var objectAttribute = new clsObjectAtt
                    {
                        ID_Attribute = AttributeItem_NewAttribute.IdAttribute,
                        ID_Class = AttributeItem_NewAttribute.IdClass,
                        ID_DataType = AttributeItem_NewAttribute.IdDataType,
                        ID_AttributeType = AttributeItem_NewAttribute.IdAttributeType,
                        ID_Object = AttributeItem_NewAttribute.IdObject,
                        OrderID = 1
                    };

                    if (AttributeItem_NewAttribute.IdDataType == localConfig.Globals.DType_Bool.GUID)
                    {
                        var boolVal = BoolNull_Val ?? false;
                        objectAttribute.Val_Bit = boolVal;
                        objectAttribute.Val_Name = boolVal.ToString();
                    }
                    else if (AttributeItem_NewAttribute.IdDataType == localConfig.Globals.DType_DateTime.GUID)
                    {
                        var dateValue = DateTimeNull_Val ?? DateTime.Now;
                        objectAttribute.Val_Datetime = dateValue;
                        objectAttribute.Val_Name = dateValue.ToString();
                    }
                    else if (AttributeItem_NewAttribute.IdDataType == localConfig.Globals.DType_Real.GUID)
                    {
                        var dblVal = DoubleNull_Val ?? 0.0;
                        objectAttribute.Val_Real = dblVal;
                        objectAttribute.Val_Name = dblVal.ToString();
                    }
                    else if (AttributeItem_NewAttribute.IdDataType == localConfig.Globals.DType_Int.GUID)
                    {
                        var lngVal = LongNull_Val ?? 0;
                        objectAttribute.Val_Lng = lngVal;
                        objectAttribute.Val_Name = lngVal.ToString();

                    }
                    else if (AttributeItem_NewAttribute.IdDataType == localConfig.Globals.DType_String.GUID)
                    {
                        var strVal = Text_Val ?? "";
                        objectAttribute.Val_String = strVal;
                        objectAttribute.Val_Name = strVal.Length > 255 ? strVal.Substring(0, 254) : strVal;
                    }

                    var result = transactionManager.do_Transaction(objectAttribute);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        
                        var interServiceMessage = new InterServiceMessage
                        {
                            ChannelId = Channels.AddedAttributes,
                            GenericParameterItems = new List<object>
                            {
                                transactionManager.OItem_Last.OItemObjectAtt.Clone()
                            }
                        };


                        webSocketServiceAgent.SendInterModMessage(interServiceMessage);

                        ReceivedIdAttribute(AttributeItem_NewAttribute.IdAttribute);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ChangedValue" && AttributeItem_NewAttribute != null)
                {
                    var value = webSocketServiceAgent.Request["Value"];
                    if (DataText_ViewId == viewIdBoolAttribute)
                    {
                        bool check;
                        if (bool.TryParse(value.ToString(), out check))
                        {
                            AttributeItem_NewAttribute.AttributeValue = check;
                        }
                    }
                    else if (DataText_ViewId == viewIdDateTimeAttribute)
                    {
                        DateTime check;
                        if (DateTime.TryParse(value.ToString(), out check))
                        {
                            AttributeItem_NewAttribute.AttributeValue = check;
                        }
                    }
                    else if (DataText_ViewId == viewIdDoubleAttribute)
                    {
                        double check;
                        if (double.TryParse(value.ToString(), out check))
                        {
                            AttributeItem_NewAttribute.AttributeValue = check;
                        }
                    }
                    else if (DataText_ViewId == viewIdLongAttribute)
                    {
                        long check;
                        if (long.TryParse(value.ToString(), out check))
                        {
                            AttributeItem_NewAttribute.AttributeValue = check;
                        }
                    }
                    else if (DataText_ViewId == viewIdStringAttribute)
                    {
                        AttributeItem_NewAttribute.AttributeValue = value.ToString();
                    }
                }



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "idAttribute")
                    {
                        var idAttributeObj = changedItem.ViewItemValue;
                        if (idAttributeObj == null) return;
                        ReceivedIdAttribute(idAttributeObj.ToString());
                    }
                    else if (changedItem.ViewItemId == "newAttributeRequest")
                    {
                        var requestItem = Newtonsoft.Json.JsonConvert.DeserializeObject<AttributeItem>(changedItem.ViewItemValue.ToString());
                        CompleteNewAttributeItem(requestItem);
                    }
                });


            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Channels.ParameterList)
            {
                var idAttributeParam = message.GenericParameterItems.Where(param => param != null).FirstOrDefault(param => param.ToString().ToLower().StartsWith("idattribute".ToLower()));
                if (idAttributeParam != null)
                {
                    var paramSplit = idAttributeParam.ToString().Split('=');
                    if (paramSplit.Count() == 2)
                    {
                        var idAttribute = paramSplit[1];
                        ReceivedIdAttribute(idAttribute);
                    }
                }
                
            }
            else if (message.ChannelId == Channels.AddAttribute)
            {
                if (DataText_ViewId != viewIdNewAttribute) return;
                if (message.GenericParameterItems != null && message.OItems != null)
                {
                    var firstParameter = message.GenericParameterItems.FirstOrDefault();
                    var selectedObject = message.OItems.FirstOrDefault();


                    if (firstParameter != null && selectedObject != null)
                    {
                        var classAttributeNode = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassAttributeNode>(firstParameter.ToString());
                        if (classAttributeNode == null) return;
                        var attributeTypeItem = elasticServiceAgent.GetOItem(classAttributeNode.IdAttributeType, localConfig.Globals.Type_AttributeType);
                        if (attributeTypeItem == null) return;
                        var classItem = elasticServiceAgent.GetOItem(classAttributeNode.IdClass, localConfig.Globals.Type_Class);
                        if (classItem == null) return;

                        NewAttribute(attributeTypeItem.GUID_Parent, 
                            attributeTypeItem.GUID, 
                            attributeTypeItem.Name, 
                            classItem.GUID,
                            classItem.Name,
                            selectedObject.GUID,
                            selectedObject.Name);
                    }
                }
            }
          
        }

        private void NewAttribute(string idDataType, 
            string idAttributeType, 
            string nameAttribute, 
            string idClass, 
            string nameClass,
            string idObject,
            string nameObject)
        {
            if (DataText_ViewId != viewIdNewAttribute) return;

            if (idDataType == localConfig.Globals.DType_Bool.GUID)
            {
                AttributeItem_NewAttribute = new Models.AttributeItem
                {
                    IdAttribute = localConfig.Globals.NewGUID,
                    IdAttributeType = idAttributeType,
                    NameAttributeType = nameAttribute,
                    AttributeValue = false,
                    IdClass = idClass,
                    NameClass = nameClass,
                    IdObject = idObject,
                    NameObject = nameObject,
                    IdDataType = localConfig.Globals.DType_Bool.GUID,
                    NameDataType = localConfig.Globals.DType_Bool.Name
                };

            }
            else if (idDataType == localConfig.Globals.DType_DateTime.GUID)
            {
                AttributeItem_NewAttribute = new Models.AttributeItem
                {
                    IdAttribute = localConfig.Globals.NewGUID,
                    IdAttributeType = idAttributeType,
                    NameAttributeType = nameAttribute,
                    AttributeValue = DateTime.Now,
                    IdClass = idClass,
                    NameClass = nameClass,
                    IdObject = idObject,
                    NameObject = nameObject,
                    IdDataType = localConfig.Globals.DType_DateTime.GUID,
                    NameDataType = localConfig.Globals.DType_DateTime.Name
                };
            }
            else if (idDataType == localConfig.Globals.DType_Int.GUID)
            {
                AttributeItem_NewAttribute = new Models.AttributeItem
                {
                    IdAttribute = localConfig.Globals.NewGUID,
                    IdAttributeType = idAttributeType,
                    NameAttributeType = nameAttribute,
                    AttributeValue = 0,
                    IdClass = idClass,
                    NameClass = nameClass,
                    IdObject = idObject,
                    NameObject = nameObject,
                    IdDataType = localConfig.Globals.DType_Int.GUID,
                    NameDataType = localConfig.Globals.DType_Int.Name
                };

            }
            else if (idDataType == localConfig.Globals.DType_Real.GUID)
            {
                AttributeItem_NewAttribute = new Models.AttributeItem
                {
                    IdAttribute = localConfig.Globals.NewGUID,
                    IdAttributeType = idAttributeType,
                    NameAttributeType = nameAttribute,
                    AttributeValue = 0.0,
                    IdClass = idClass,
                    NameClass = nameClass,
                    IdObject = idObject,
                    NameObject = nameObject,
                    IdDataType = localConfig.Globals.DType_Real.GUID,
                    NameDataType = localConfig.Globals.DType_Real.Name
                };
            }
            else if (idDataType == localConfig.Globals.DType_String.GUID)
            {
                AttributeItem_NewAttribute = new Models.AttributeItem
                {
                    IdAttribute = localConfig.Globals.NewGUID,
                    IdAttributeType = idAttributeType,
                    NameAttributeType = nameAttribute,
                    AttributeValue = "",
                    IdClass = idClass,
                    NameClass = nameClass,
                    IdObject = idObject,
                    NameObject = nameObject,
                    IdDataType = localConfig.Globals.DType_String.GUID,
                    NameDataType = localConfig.Globals.DType_String.Name
                };
            }
        }

        private void ReceivedIdAttribute(string idAttribute)
        {
            if (objectAttribute != null && objectAttribute.ID_Attribute == idAttribute) return;

            objectAttribute = elasticServiceAgent.GetObjectAttribute(idAttribute);

            relatedObject = new clsOntologyItem
            {
                GUID = objectAttribute.ID_Object,
                Name = objectAttribute.Name_Object,
                GUID_Parent = objectAttribute.ID_Class,
                Type = localConfig.Globals.Type_Object
            };

            relatedClass = elasticServiceAgent.GetOItem(relatedObject.GUID_Parent, localConfig.Globals.Type_Class);

            relatedAttributeType = elasticServiceAgent.GetOItem(objectAttribute.ID_AttributeType, localConfig.Globals.Type_AttributeType);

            if (objectAttribute == null || relatedObject == null || relatedClass == null || relatedAttributeType == null) return;

            Label_ItemIdentification = System.Web.HttpUtility.HtmlEncode(relatedClass.Name) + @"\" + System.Web.HttpUtility.HtmlEncode(relatedObject.Name);



            if (DataText_ViewId == viewIdLongAttribute && objectAttribute.ID_DataType == localConfig.Globals.DType_Int.GUID)
            {
                Label_AttributeType = relatedAttributeType.Name + ":";
                LongNull_Val = objectAttribute.Val_Lng;
                IsEnabled_ValNumeric = true;
            }
            else if (DataText_ViewId == viewIdBoolAttribute && objectAttribute.ID_DataType == localConfig.Globals.DType_Bool.GUID)
            {
                Label_AttributeTypeCheckbox = relatedAttributeType.Name;
                BoolNull_Val = objectAttribute.Val_Bool;
                IsEnabled_ValCheckbox = true;
            }
            if (DataText_ViewId == viewIdDateTimeAttribute && objectAttribute.ID_DataType == localConfig.Globals.DType_DateTime.GUID)
            {
                Label_AttributeType = relatedAttributeType.Name + ":";
                DateTimeNull_Val = objectAttribute.Val_Datetime;
                IsEnabled_DateTimePicker = true;
            }
            else if (DataText_ViewId == viewIdDoubleAttribute && objectAttribute.ID_DataType == localConfig.Globals.DType_Real.GUID)
            {
                Label_AttributeType = relatedAttributeType.Name + ":";
                DoubleNull_Val = objectAttribute.Val_Double;
                IsEnabled_ValNumeric = true;
            }
            else if (DataText_ViewId == viewIdStringAttribute && objectAttribute.ID_DataType == localConfig.Globals.DType_String.GUID)
            {
                Label_AttributeType = relatedAttributeType.Name + ":";
                Text_Val = objectAttribute.Val_String;
                IsEnabled_ValTextarea = true;
            }
        }

      
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
