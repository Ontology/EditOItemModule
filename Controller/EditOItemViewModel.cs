﻿using EditOItemModule.Models;
using EditOItemModule.Notifications;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditOItemModule.Controller
{
    public class EditOItemViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string url_Menu;
        [ViewModel(Send = true)]
        public string Url_Menu
        {
            get { return url_Menu; }
            set
            {
                if (url_Menu == value) return;

                url_Menu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Menu);

            }
        }

        private bool isvisible_Cancel;
        [ViewModel(Send = true, ViewItemId = "cancel", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Cancel
        {
            get { return isvisible_Cancel; }
            set
            {
                if (isvisible_Cancel == value) return;

                isvisible_Cancel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Cancel);

            }
        }

        private bool isenabled_Cancel;
        [ViewModel(Send = true, ViewItemId = "cancel", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Cancel
        {
            get { return isenabled_Cancel; }
            set
            {
                if (isenabled_Cancel == value) return;

                isenabled_Cancel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Cancel);

            }
        }

        private string label_Cancel;
        [ViewModel(Send = true, ViewItemId = "cancel", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_Cancel
        {
            get { return label_Cancel; }
            set
            {
                if (label_Cancel == value) return;

                label_Cancel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Cancel);

            }
        }

        private bool isvisible_Save;
        [ViewModel(Send = true, ViewItemId = "saveItem", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Save
        {
            get { return isvisible_Save; }
            set
            {
                if (isvisible_Save == value) return;

                isvisible_Save = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Save);

            }
        }

        private bool isenabled_Save;
        [ViewModel(Send = true, ViewItemId = "saveItem", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Save
        {
            get { return isenabled_Save; }
            set
            {
                if (isenabled_Save == value) return;

                isenabled_Save = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Save);

            }
        }

        private string label_Save;
        [ViewModel(Send = true, ViewItemId = "saveItem", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_Save
        {
            get { return label_Save; }
            set
            {
                if (label_Save == value) return;

                label_Save = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Save);

            }
        }

        
        private bool isenabled_DropDownItems;
        [ViewModel(Send = true, ViewItemId = "dropDownItemType", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_DropDownItems
        {
            get { return isenabled_DropDownItems; }
            set
            {
                if (isenabled_DropDownItems == value) return;

                isenabled_DropDownItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_DropDownItems);

            }
        }

        private bool isvisible_DropDownItems;
        [ViewModel(Send = true, ViewItemId = "dropDownItemType", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_DropDownItems
        {
            get { return isvisible_DropDownItems; }
            set
            {
                if (isvisible_DropDownItems == value) return;

                isvisible_DropDownItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_DropDownItems);

            }
        }

        private List<string> stringList_DropDownItems;
        [ViewModel(Send = true, ViewItemId = "dropDownItemType", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.Content)]
        public List<string> StringList_DropDownItems
        {
            get { return stringList_DropDownItems; }
            set
            {
                if (stringList_DropDownItems == value) return;

                stringList_DropDownItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_StringList_DropDownItems);

            }
        }

        private bool isvisible_ItemType;
        [ViewModel(Send = true, ViewItemId = "lblItemType", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ItemType
        {
            get { return isvisible_ItemType; }
            set
            {
                if (isvisible_ItemType == value) return;

                isvisible_ItemType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ItemType);

            }
        }

        private string label_ItemType;
        [ViewModel(Send = true, ViewItemId = "lblItemType", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_ItemType
        {
            get { return label_ItemType; }
            set
            {
                if (label_ItemType == value) return;

                label_ItemType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ItemType);

            }
        }

        private string id_ItemType;
        [ViewModel(Send = true, ViewItemId = "dropDownItemType", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.SelectedIndex)]
        public string Id_ItemType
        {
            get { return id_ItemType; }
            set
            {
                if (id_ItemType == value) return;

                id_ItemType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Id_ItemType);

            }
        }

        private JqxColumnList columnlist_Grid;
        [ViewModel(Send = true, ViewItemId = "itemGrid", ViewItemClass = ViewItemClass.Grid, ViewItemType = ViewItemType.ColumnConfig)]
        public JqxColumnList ColumnList_Grid
        {
            get { return columnlist_Grid; }
            set
            {
                if (columnlist_Grid == value) return;

                columnlist_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnList_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemId = "itemGrid", ViewItemClass = ViewItemClass.Grid, ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private bool isvisible_Parent;
        [ViewModel(Send = true, ViewItemId = "lblParent", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Parent
        {
            get { return isvisible_Parent; }
            set
            {
                if (isvisible_Parent == value) return;

                isvisible_Parent = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Parent);

            }
        }

        private string text_ParentText;
        [ViewModel(Send = true, ViewItemId = "inpParent", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Content)]
        public string Text_ParentText
        {
            get { return text_ParentText; }
            set
            {
                if (text_ParentText == value) return;

                text_ParentText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_ParentText);

            }
        }

        private bool isenabled_ParentText;
        [ViewModel(Send = true, ViewItemId = "inpParent", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ParentText
        {
            get { return isenabled_ParentText; }
            set
            {
                if (isenabled_ParentText == value) return;

                isenabled_ParentText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParentText);

            }
        }

        private bool isvisible_ParentText;
        [ViewModel(Send = true, ViewItemId = "inpParent", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ParentText
        {
            get { return isvisible_ParentText; }
            set
            {
                if (isvisible_ParentText == value) return;

                isvisible_ParentText = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ParentText);

            }
        }

        private bool ischecked_Class;
        [ViewModel(Send = true, ViewItemId = "inpParent", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_Class
        {
            get { return ischecked_Class; }
            set
            {
                if (ischecked_Class == value) return;

                ischecked_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_Class);

            }
        }

        private bool isvisible_Class;
        [ViewModel(Send = true, ViewItemId = "listenClass", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Class
        {
            get { return isvisible_Class; }
            set
            {
                if (isvisible_Class == value) return;

                isvisible_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Class);

            }
        }

        private ImageLabel iconlabel_Class;
        [ViewModel(Send = true, ViewItemId = "listenClass", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.IconLabel)]
        public ImageLabel IconLabel_Class
        {
            get { return iconlabel_Class; }
            set
            {
                if (iconlabel_Class == value) return;

                iconlabel_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IconLabel_Class);

            }
        }

        private bool isenabled_Class;
        [ViewModel(Send = true, ViewItemId = "listenClass", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Class
        {
            get { return isenabled_Class; }
            set
            {
                if (isenabled_Class == value) return;

                isenabled_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Class);

            }
        }

        private string label_Parent;
        [ViewModel(Send = true, ViewItemId = "lblParent", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_Parent
        {
            get { return label_Parent; }
            set
            {
                if (label_Parent == value) return;

                label_Parent = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Parent);

            }
        }

        private List<EditItem> edititemlist_EditItems = new List<EditItem>();
        [ViewModel(Send = false)]
        public List<EditItem> EditItemList_EditItems
        {
            get { return edititemlist_EditItems; }
            set
            {
                if (edititemlist_EditItems == value) return;

                edititemlist_EditItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_EditItemList_EditItems);

            }
        }

        private EditItem edititem_EditItemDelete;
        [ViewModel(Send = true)]
        public EditItem EditItem_EditItemDelete
        {
            get { return edititem_EditItemDelete; }
            set
            {
                if (edititem_EditItemDelete == value) return;

                edititem_EditItemDelete = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_EditItem_EditItemDelete);

            }
        }

        private EditItem edititem_EditItemCreate;
        [ViewModel(Send = true)]
        public EditItem EditItem_EditItemCreate
        {
            get { return edititem_EditItemCreate; }
            set
            {
                if (edititem_EditItemCreate == value) return;

                edititem_EditItemCreate = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_EditItem_EditItemCreate);

            }
        }

        private EditItem edititem_EditItemUpdate;
        [ViewModel(Send = true)]
        public EditItem EditItem_EditItemUpdate
        {
            get { return edititem_EditItemUpdate; }
            set
            {
                if (edititem_EditItemUpdate == value) return;

                edititem_EditItemUpdate = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_EditItem_EditItemUpdate);

            }
        }

        private EditItem edititem_SelectedItem;
        [ViewModel(Send = true)]
        public EditItem EditItem_SelectedItem
        {
            get { return edititem_SelectedItem; }
            set
            {
                if (edititem_SelectedItem == value) return;

                edititem_SelectedItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_EditItem_SelectedItem);

            }
        }

        private bool isenabled_Item = true;
        [ViewModel(Send = true, ViewItemId = "listenItem", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Item
        {
            get { return isenabled_Item; }
            set
            {
                if (isenabled_Item == value) return;

                isenabled_Item = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Item);

            }
        }

        private bool isvisible_Item;
        [ViewModel(Send = true, ViewItemId = "listenItem", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Item
        {
            get { return isvisible_Item; }
            set
            {
                if (isvisible_Item == value) return;

                isvisible_Item = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Item);

            }
        }

        private string label_Item;
        [ViewModel(Send = true, ViewItemId = "listenItem", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Content)]
        public string Label_Item
        {
            get { return label_Item; }
            set
            {
                if (label_Item == value) return;

                label_Item = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Item);

            }
        }

        private bool ischecked_Item;
        [ViewModel(Send = true, ViewItemId = "listenItem", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_Item
        {
            get { return ischecked_Item; }
            set
            {
                if (ischecked_Item == value) return;

                ischecked_Item = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_Item);

            }
        }

        private bool isenabled_NewGuid;
        [ViewModel(Send = true, ViewItemId = "newGuid", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NewGuid
        {
            get { return isenabled_NewGuid; }
            set
            {
                if (isenabled_NewGuid == value) return;

                isenabled_NewGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NewGuid);

            }
        }

        private bool isvisible_NewGuid;
        [ViewModel(Send = true, ViewItemId = "newGuid", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_NewGuid
        {
            get { return isvisible_NewGuid; }
            set
            {
                if (isvisible_NewGuid == value) return;

                isvisible_NewGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_NewGuid);

            }
        }

        private string text_Guid;
        [ViewModel(Send = true, ViewItemId = "inpGuid", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Content)]
        public string Text_Guid
        {
            get { return text_Guid; }
            set
            {
                if (text_Guid == value) return;

                text_Guid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Guid);

            }
        }

        private bool isenabled_Guid;
        [ViewModel(Send = true, ViewItemId = "inpGuid", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Guid
        {
            get { return isenabled_Guid; }
            set
            {
                if (isenabled_Guid == value) return;

                isenabled_Guid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Guid);

            }
        }

        private bool isvisible_Guid;
        [ViewModel(Send = true, ViewItemId = "inpGuid", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Guid
        {
            get { return isvisible_Guid; }
            set
            {
                if (isvisible_Guid == value) return;

                isvisible_Guid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Guid);

            }
        }

        private string label_GuidLbl;
        [ViewModel(Send = true, ViewItemId = "lblGuid", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_GuidLbl
        {
            get { return label_GuidLbl; }
            set
            {
                if (label_GuidLbl == value) return;

                label_GuidLbl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_GuidLbl);

            }
        }

        private bool isvisible_GuidLbl;
        [ViewModel(Send = true, ViewItemId = "lblGuid", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_GuidLbl
        {
            get { return isvisible_GuidLbl; }
            set
            {
                if (isvisible_GuidLbl == value) return;

                isvisible_GuidLbl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_GuidLbl);

            }
        }

        private string label_NewGuid;
        [ViewModel(Send = true, ViewItemId = "newGuid", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_NewGuid
        {
            get { return label_NewGuid; }
            set
            {
                if (label_NewGuid == value) return;

                label_NewGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_NewGuid);

            }
        }


        private bool ischecked_ParentItem;
        [ViewModel(Send = true, ViewItemId = "listenParent", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_ParentItem
        {
            get { return ischecked_ParentItem; }
            set
            {
                if (ischecked_ParentItem == value) return;

                ischecked_ParentItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_ParentItem);

            }
        }

        private string label_ParentItem;
        [ViewModel(Send = true, ViewItemId = "listenParent", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Content)]
        public string Label_ParentItem
        {
            get { return label_ParentItem; }
            set
            {
                if (label_ParentItem == value) return;

                label_ParentItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ParentItem);

            }
        }

        private bool isenabled_ParentItem;
        [ViewModel(Send = true, ViewItemId = "listenParent", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ParentItem
        {
            get { return isenabled_ParentItem; }
            set
            {
                if (isenabled_ParentItem == value) return;

                isenabled_ParentItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParentItem);

            }
        }

        private bool isvisible_ParentItem;
        [ViewModel(Send = true, ViewItemId = "listenParent", ViewItemClass = ViewItemClass.ToggleButton, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ParentItem
        {
            get { return isvisible_ParentItem; }
            set
            {
                if (isvisible_ParentItem == value) return;

                isvisible_ParentItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ParentItem);

            }
        }

        private string label_DeleteItem;
        [ViewModel(Send = true, ViewItemId = "deleterowbutton", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_DeleteItem
        {
            get { return label_DeleteItem; }
            set
            {
                if (label_DeleteItem == value) return;

                label_DeleteItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_DeleteItem);

            }
        }

        private bool isenabled_DeleteItem;
        [ViewModel(Send = true, ViewItemId = "deleterowbutton", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_DeleteItem
        {
            get { return isenabled_DeleteItem; }
            set
            {
                if (isenabled_DeleteItem == value) return;

                isenabled_DeleteItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_DeleteItem);

            }
        }

        private bool isvisible_DeleteItem;
        [ViewModel(Send = true, ViewItemId = "deleterowbutton", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_DeleteItem
        {
            get { return isvisible_DeleteItem; }
            set
            {
                if (isvisible_DeleteItem == value) return;

                isvisible_DeleteItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_DeleteItem);

            }
        }

        private string label_NewItem;
        [ViewModel(Send = true, ViewItemId = "addrowbutton", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_NewItem
        {
            get { return label_NewItem; }
            set
            {
                if (label_NewItem == value) return;

                label_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_NewItem);

            }
        }

        private bool isenabled_NewItem;
        [ViewModel(Send = true, ViewItemId = "addrowbutton", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NewItem
        {
            get { return isenabled_NewItem; }
            set
            {
                if (isenabled_NewItem == value) return;

                isenabled_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NewItem);

            }
        }

        private bool isvisible_NewItem;
        [ViewModel(Send = true, ViewItemId = "addrowbutton", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_NewItem
        {
            get { return isvisible_NewItem; }
            set
            {
                if (isvisible_NewItem == value) return;

                isvisible_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_NewItem);

            }
        }

        private List<EditItem> edititemlist_ChangedEditItems = new List<EditItem>();
        [ViewModel(Send = true, ViewItemId = "itemGrid", ViewItemClass = ViewItemClass.Grid, ViewItemType = ViewItemType.Content)]
        public List<EditItem> EditItemList_ChangedEditItems
        {
            get { return edititemlist_ChangedEditItems; }
            set
            {
                if (edititemlist_ChangedEditItems == value) return;

                edititemlist_ChangedEditItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_EditItemList_ChangedEditItems);

            }
        }

        private string text_SaveMessage;
        [ViewModel(Send = true, ViewItemId = "message", ViewItemClass = ViewItemClass.Other, ViewItemType = ViewItemType.Content)]
        public string Text_SaveMessage
        {
            get { return text_SaveMessage; }
            set
            {
                if (text_SaveMessage == value) return;

                text_SaveMessage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_SaveMessage);

            }
        }

        private bool isenabled_ApplyItems;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "applyItem", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ApplyItems
        {
            get { return isenabled_ApplyItems; }
            set
            {

                isenabled_ApplyItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ApplyItems);

            }
        }

        private bool isvisible_ApplyItems;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "applyItem", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ApplyItems
        {
            get { return isvisible_ApplyItems; }
            set
            {

                isvisible_ApplyItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ApplyItems);

            }
        }

        private string label_ApplyItems;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "applyItem", ViewItemType = ViewItemType.Content)]
        public string Label_ApplyItems
        {
            get { return label_ApplyItems; }
            set
            {
                if (label_ApplyItems == value) return;

                label_ApplyItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ApplyItems);

            }
        }

        private string datatext_ExitMessage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "exitMessage", ViewItemType = ViewItemType.Other)]
        public string DataText_ExitMessage
        {
            get { return datatext_ExitMessage; }
            set
            {
                if (datatext_ExitMessage == value) return;

                datatext_ExitMessage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ExitMessage);

            }
        }

      
        private bool isinitializing_Controller;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "controllerIsInitalizing", ViewItemType = ViewItemType.Other)]
        public bool IsInitializing_Controller
        {
            get { return isinitializing_Controller; }
            set
            {
                if (isinitializing_Controller == value) return;

                isinitializing_Controller = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsInitializing_Controller);

            }
        }

       

    }
}
