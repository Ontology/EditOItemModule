﻿using EditOItemModule.Models;
using EditOItemModule.Services;
using EditOItemModule.Translations;
using Newtonsoft.Json.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Attributes;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace EditOItemModule.Controller
{
    public class EditOItemController : EditOItemViewModel, IViewController
    {
        private object checkLocker = new object();
        private WebsocketServiceAgent webSocketServiceAgent;

        private ElasticServiceAgent elasticServiceAgent;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        private List<MenuEntry> menuEntries = new List<MenuEntry>();
        private MenuEntry editEntry;
        private MenuEntry passwordEntry;

        private string itemType;
        private string itemId;

        private clsOntologyItem selectedClass;
        private clsOntologyItem selectedObject;
        private clsOntologyItem selectedRelationType;
        private clsOntologyItem selectedAttributeType;
        private clsOntologyItem dataType;
        private clsOntologyItem parentClass;

        private bool setViewProperties = false;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public EditOItemController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }



            PropertyChanged += EditOItemController_PropertyChanged;
        }

        private void EditOItemController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));


            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Id_ItemType)
            {
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_Class)
            {
                
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_Item)
            {
                
            }

            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Text_GuidText)
            {
                
            }

            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Text_ParentText)
            {
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsInitializing_Controller)
            {
                if (!IsInitializing_Controller)
                {
                    if (selectedObject != null)
                    {
                        SelectedObject(selectedObject);
                    }
                    else if (parentClass != null)
                    {
                        TestParent(parentClass.GUID);
                    }
                    else if (selectedObject == null && selectedClass == null)
                    {
                        var parentId = webSocketServiceAgent.Context.QueryString["ParentId"];
                        if (!string.IsNullOrEmpty(parentId))
                        {
                            TestParent(parentId);
                        }
                    }
                }
            }

            ValidateControls();

        }

        private void ValidateControls()
        {
            
            int selectedIndex;
            if (int.TryParse(Id_ItemType, out selectedIndex))
            {
                if (StringList_DropDownItems != null && StringList_DropDownItems.Count > selectedIndex)
                {
                    var selectedType = StringList_DropDownItems[selectedIndex];

                    if (selectedType == localConfig.Globals.Type_Object)
                    {
                        if (parentClass != null)
                        {
                            IsEnabled_NewItem = true;   
                            

                        }
                    }
                    else if (selectedType == localConfig.Globals.Type_Class)
                    {

                    }
                }
            }
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;



            elasticServiceAgent = new ElasticServiceAgent(localConfig);
            elasticServiceAgent.PropertyChanged += ElasticServiceAgent_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            elasticServiceAgent.StopRead();
            webSocketServiceAgent.RemoveAllResources();
            elasticServiceAgent = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            setViewProperties = true;
            


            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
            
            IsInitializing_Controller = true;
            webSocketServiceAgent.SetVisibility(true);
            webSocketServiceAgent.SetEnable(false);


            Label_Save = translationController.Label_Save;
            Label_Cancel = translationController.Label_Cancel;
            Label_ItemType = translationController.Label_ItemType;
            Label_Parent = translationController.Label_Parent;
            Label_GuidLbl = translationController.Label_Guid;
            Label_NewGuid = translationController.Label_NewGuid;
            Label_ApplyItems = translationController.Label_Apply;

            var dropDownItems = new List<string>
            {
                localConfig.Globals.Type_AttributeType,
                localConfig.Globals.Type_RelationType,
                localConfig.Globals.Type_Object,
                localConfig.Globals.Type_Class
            };

            StringList_DropDownItems = dropDownItems;
            Id_ItemType = "2";

            IsVisible_Cancel = false;
            IsEnabled_DropDownItems = true;
            IsEnabled_ParentText = false;
            IsEnabled_ApplyItems = false;

            IconLabel_Class = new ImageLabel
            {
                FontIcon = "fa-globe"
            };



            itemId = webSocketServiceAgent.Context.QueryString[Notifications.NotifyChanges.RequestKey_ItemId];
            itemType = webSocketServiceAgent.Context.QueryString[Notifications.NotifyChanges.RequestKey_ItemType];

            if (string.IsNullOrEmpty(itemType))
            {
                IsSuccessful_Login = false;
                return;
            }

            

            editEntry = new MenuEntry
            {
                Id = "edit",
                Name = translationController.Text_MenuEntryEdit,
                IsVisible = true,
                IsEnabled = true
            };

            passwordEntry = new MenuEntry
            {
                Id = "password",
                Name = translationController.Text_MenuEntryPassword,
                IsVisible = true,
                IsEnabled = true,
                ParentEntry = editEntry
            };

            menuEntries.Add(editEntry);
            menuEntries.Add(passwordEntry);

            var fileNameContextMenuDatasource = Guid.NewGuid().ToString() + ".json";
            var fileNameContextMenuData = Guid.NewGuid().ToString() + ".json";
            var sessionFileDataSource = webSocketServiceAgent.RequestWriteStream(fileNameContextMenuDatasource);

            if (sessionFileDataSource == null) return;

            var jqxDataSource = new JqxDataSource(JsonDataType.Json)
            {
                datafields = new List<DataField>
                     {
                         new DataField(DataFieldType.String)
                         {
                             name = "Id"
                         },
                         new DataField(DataFieldType.String)
                         {
                             name = "Name"
                         },
                         new DataField(DataFieldType.String)
                         {
                             name = "Html"
                         },
                         new DataField(DataFieldType.String)
                         {
                             name = "ParentId"
                         }
                     },
                id = "Id",
                localdata = menuEntries
            };


            var jsonToWrite = Newtonsoft.Json.JsonConvert.SerializeObject(jqxDataSource);
            using (sessionFileDataSource.StreamWriter)
            {
                sessionFileDataSource.StreamWriter.Write(jsonToWrite);
            }


            Url_Menu = sessionFileDataSource.FileUri.AbsoluteUri;

            EditItemList_EditItems = new List<Models.EditItem>();

            JqxDataSource_Grid = new JqxDataSource(JsonDataType.Json)
            {
                datafields = new List<DataField>
                {
                    new DataField(DataFieldType.String)
                    {
                        name = "Guid"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "Name"
                    },
                    new DataField(DataFieldType.Bool)
                    {
                        name = "Listen"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "ItemType"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "GuidParent"
                    }
                    ,
                    new DataField(DataFieldType.String)
                    {
                        name = "NameParent"
                    },
                    new DataField(DataFieldType.Bool)
                    {
                        name = "Comment"
                    }
                },
                id = "Guid",
                localdata = EditItemList_EditItems
            };

            ColumnList_Grid = new JqxColumnList
            {
                ColumnList = new List<JqxColumnAttribute>
                {
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Checkbox)
                    {
                        text = translationController.Col_Save,
                        datafield = "Save",
                        editable = true,
                        width = 50
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = translationController.Col_Guid,
                        datafield = "Guid",
                        editable = false,
                        width = 100
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = translationController.Col_Name,
                        datafield = "Name",
                        editable = true
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = translationController.Col_ItemType,
                        datafield = "ItemType",
                        editable = false,
                        width = 100
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "GuidParent",
                        datafield = "GuidParent",
                        editable = false,
                        hidden = true
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = translationController.Col_NameParent,
                        datafield = "NameParent",
                        editable = false,
                        width = 150
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Comment",
                        datafield = "Comment",
                        editable = false,
                        width = 150
                    }

                }
            };




            webSocketServiceAgent.SendModel();
            webSocketServiceAgent.SendCommand("ModelSended");
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ElasticServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                



            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

                private void AddItem()
        {
            IsEnabled_DeleteItem = false;
            EditItem_EditItemCreate = new EditItem
            {
                Save = true,
                Guid = localConfig.Globals.NewGUID,
                Name = "",
                ItemType = itemType,
                GuidParent = parentClass != null ? parentClass.GUID : null,
                NameParent = parentClass != null ? parentClass.Name : null,
                Comment = ""
            };
            EditItemList_EditItems.Add(EditItem_EditItemCreate);
        }

        private void SelectedRow()
        {
            var guid = webSocketServiceAgent.Request["Guid"].ToString();
            var editItem = EditItemList_EditItems.FirstOrDefault(editItm => editItm.Guid == guid);

            if (editItem == null)
            {
                ViewValidation();
                return;
            }

            IsEnabled_DeleteItem = true;
            EditItem_SelectedItem = editItem;
        }

        private void RemovedRow()
        {
            var guid = webSocketServiceAgent.Request["Guid"].ToString();
            var editItem = EditItemList_EditItems.FirstOrDefault(editItm => editItm.Guid == guid);

            if (editItem == null)
            {
                ViewValidation();
                return;
            }

            EditItem_EditItemDelete = editItem;
            IsEnabled_DeleteItem = false;
            EditItemList_EditItems.Remove(editItem);
            EditItemList_ChangedEditItems.Remove(editItem);
        }

        private void GridInitialized()
        {
            IsEnabled_DeleteItem = false;

            IsVisible_Item = true;
            IsEnabled_Item = true;
            Label_Item = translationController.Label_Listen;

            IsVisible_ParentItem = true;
            IsEnabled_ParentItem = true;
            Label_ParentItem = translationController.Label_ListenParent;

            IsVisible_NewItem = true;
            IsEnabled_NewItem = false;
            Label_NewItem = translationController.Label_NewItem;

            IsVisible_DeleteItem = true;
            IsEnabled_DeleteItem = false;
            Label_DeleteItem = translationController.Label_DeleteItem;

            IsVisible_ParentText = true;
            IsEnabled_ParentText = false;
        }

        private void SaveItems()
        {
            var result = elasticServiceAgent.SaveEditItems(EditItemList_EditItems);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var attributeTypes = EditItemList_EditItems.Where(editItm => editItm.ItemType == localConfig.Globals.Type_AttributeType).Select(itm => new clsOntologyItem
                {
                   GUID = itm.Guid,
                   Name = itm.Name,
                   GUID_Parent = itm.GuidParent,
                   Type = localConfig.Globals.Type_AttributeType
                }).ToList();

                var relationTypes = EditItemList_EditItems.Where(editItm => editItm.ItemType == localConfig.Globals.Type_RelationType).Select(itm => new clsOntologyItem
                {
                    GUID = itm.Guid,
                    Name = itm.Name,
                    Type = localConfig.Globals.Type_RelationType
                }).ToList();

                var objects = EditItemList_EditItems.Where(editItm => editItm.ItemType == localConfig.Globals.Type_Object).Select(itm => new clsOntologyItem
                {
                    GUID = itm.Guid,
                    Name = itm.Name,
                    GUID_Parent = itm.GuidParent,
                    Type = localConfig.Globals.Type_Object
                }).ToList();

                var classes = EditItemList_EditItems.Where(editItm => editItm.ItemType == localConfig.Globals.Type_Class).Select(itm => new clsOntologyItem
                {
                    GUID = itm.Guid,
                    Name = itm.Name,
                    GUID_Parent = itm.GuidParent,
                    Type = localConfig.Globals.Type_Class
                }).ToList();

                if (attributeTypes.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AddedAttributeTypes,
                        OItems = attributeTypes
                    };
                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (relationTypes.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AddedRelationTypes,
                        OItems = relationTypes
                    };
                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (classes.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AddedClasses,
                        OItems = classes
                    };
                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (objects.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AddedObjects,
                        OItems = objects
                    };
                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }


                Text_SaveMessage = "Success!";
            }
            else
            {
                Text_SaveMessage = "Error!";
            }
        }

        private void ApplyItems()
        {
            var attributeTypes = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_AttributeType).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                GUID_Parent = editItem.GuidParent,
                Type = localConfig.Globals.Type_AttributeType
            }).ToList();

            if (attributeTypes != null && attributeTypes.Any())
            {
                var interServiceMessage = new InterServiceMessage
                {
                    ChannelId = Channels.AppliedAttributeTypes,
                    OItems = attributeTypes
                };
                webSocketServiceAgent.SendInterModMessage(interServiceMessage);
            }

            var relationTypes = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_RelationType).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                Type = localConfig.Globals.Type_RelationType
            }).ToList();

            if (relationTypes != null && relationTypes.Any())
            {
                var interServiceMessage = new InterServiceMessage
                {
                    ChannelId = Channels.AppliedRelationTypes,
                    OItems = relationTypes
                };
                webSocketServiceAgent.SendInterModMessage(interServiceMessage);
            }

            var classes = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_Class).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                GUID_Parent = editItem.GuidParent,
                Type = localConfig.Globals.Type_Class
            }).ToList();


            var objects = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_Object).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                GUID_Parent = editItem.GuidParent,
                Type = localConfig.Globals.Type_Object
            }).ToList();

            if (objects != null && objects.Any())
            {
                var interServiceMessage = new InterServiceMessage
                {
                    ChannelId = Channels.AppliedObjects,
                    OItems = objects
                };
                webSocketServiceAgent.SendInterModMessage(interServiceMessage);
            }
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                
                Initialize();
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                var itemType = "";
                int selectedIndex;
                if (int.TryParse(Id_ItemType, out selectedIndex))
                {
                    itemType = StringList_DropDownItems[selectedIndex];
                }


                if (!string.IsNullOrEmpty(itemType))
                {
                    if (webSocketServiceAgent.Command_RequestedCommand == "AddItem")
                    {
                        AddItem();
                    }
                    else if (webSocketServiceAgent.Command_RequestedCommand == "SelectedRow")
                    {
                        SelectedRow();
                    }
                    else if (webSocketServiceAgent.Command_RequestedCommand == "RemoveRow")
                    {
                        RemovedRow();
                    }
                    else if (webSocketServiceAgent.Command_RequestedCommand == "GridInitialized")
                    {
                        GridInitialized();

                    }
                    else if (webSocketServiceAgent.Command_RequestedCommand == "SaveItems")
                    {
                        SaveItems();
                    }
                    else if (webSocketServiceAgent.Command_RequestedCommand == "ApplyItems")
                    {
                        ApplyItems();
                    }
                    else if (webSocketServiceAgent.Command_RequestedCommand == "AckModelSended")
                    {
                        IsInitializing_Controller = false;
                    }


                }
                


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                GetChangedViewItems();

                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ViewArguments)
            {
                
            }

            ViewValidation();
            
        }

        private void CheckViewProperties()
        {
            var goOn = true;
            
            lock (checkLocker)
            {
                goOn = setViewProperties;
                setViewProperties = false;
            }

            if (!goOn) return;

            var objectParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("object"));

            if (objectParam != null)
            {

                var oItemObject = elasticServiceAgent.GetOItem(objectParam.Value, localConfig.Globals.Type_Object);

                if (oItemObject == null) return;
                selectedObject = oItemObject;

                return;
            }

            var classParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("class"));

            if (classParam != null)
            {
                var oItemClass = elasticServiceAgent.GetOItem(classParam.Value, localConfig.Globals.Type_Class);

                if (oItemClass == null) return;

                TestParent(oItemClass.GUID);
                return;
            }
            
        }

        private void GetChangedViewItems()
        {
            webSocketServiceAgent.ChangedViewItems.ForEach(changeItem =>
            {
                if (changeItem.ViewItemId == Notifications.NotifyChanges.ViewModel_EditItem_EditItemUpdate)
                {
                    var itemType = "";
                    int selectedIndex;
                    if (int.TryParse(Id_ItemType, out selectedIndex))
                    {
                        itemType = StringList_DropDownItems[selectedIndex];
                    }

                    var editItemString = changeItem.ViewItemValue;
                    if (editItemString == null)
                    {
                        ViewValidation();
                        return;
                    }
                    var editItem = Newtonsoft.Json.JsonConvert.DeserializeObject<EditItem>(editItemString.ToString());

                    var editItemInList = EditItemList_EditItems.First(editItm => editItm.Guid == editItem.Guid);

                    editItemInList.Name = editItem.Name;
                }
                else if (changeItem.ViewItemId == "listenItem" && changeItem.ViewItemType == ViewItemType.Checked.ToString())
                {
                    IsChecked_Item = (bool)changeItem.ViewItemValue;
                }
                else if (changeItem.ViewItemId == "dropDownItemType" && changeItem.ViewItemType == ViewItemType.SelectedIndex.ToString())
                {
                    parentClass = null;
                    Text_ParentText = "";
                    CheckViewProperties();
                }
                
                
            });
            
        }

        private void ViewValidation()
        {
            var attributeTypes = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_AttributeType).ToList();
            var relationTypes = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_RelationType).ToList();
            var classes = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_Class).ToList();
            var objects = EditItemList_EditItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_Object).ToList();

            if (attributeTypes.Count + relationTypes.Count + classes.Count + objects.Count != EditItemList_EditItems.Count)
            {
                IsEnabled_Save = false;
                return;
            }

            ValidateExitMessage(EditItemList_EditItems);
            ValidateAndUpdateItemList(attributeTypes, localConfig.Globals.Type_AttributeType, false);
            ValidateAndUpdateItemList(relationTypes, localConfig.Globals.Type_RelationType, false);
            ValidateAndUpdateItemList(classes, localConfig.Globals.Type_Class, false);
            ValidateAndUpdateItemList(objects, localConfig.Globals.Type_Object, true);

            if (!attributeTypes.Any() && !relationTypes.Any() && !classes.Any() && !objects.Any())
            {
                IsEnabled_Save = false;
                return;
            }
            var notAllowed = EditItemList_EditItems.Any(editItem => editItem.SaveAllowed == false);
            IsEnabled_Save = !notAllowed;
            var invalidItems = EditItemList_EditItems.Where(editItem => !editItem.SaveAllowed || !string.IsNullOrEmpty(editItem.Comment)).ToList();
            if (!EditItemList_ChangedEditItems.Any())
            {
                EditItemList_ChangedEditItems = invalidItems;
            }
            else
            {
                var resetItems = (from changedItem in EditItemList_ChangedEditItems
                                  join invalidItem in invalidItems on changedItem equals invalidItem into invalidItems1
                                  from invalidItem in invalidItems1.DefaultIfEmpty()
                                  where invalidItem == null
                                  select changedItem).ToList();

                resetItems.ForEach(resetItem =>
                {
                    resetItem.SaveAllowed = true;
                    resetItem.Comment = "";
                });

                var changedItems = resetItems;
                changedItems.AddRange(invalidItems);

                EditItemList_ChangedEditItems = changedItems;
            }

            IsEnabled_ApplyItems = (attributeTypes.Any() || relationTypes.Any() || classes.Any() || objects.Any());

            
            
        }

        private void ValidateExitMessage(List<EditItem> editItems)
        {
            if (!editItems.Any())
            {
                DataText_ExitMessage = "";
            }
            else
            {
                DataText_ExitMessage = translationController.Label_ExitMessage;
            }
        }

        private void ValidateAndUpdateItemList(List<EditItem> editItems, string type, bool nameDoubleAllowed)
        {
            editItems.ForEach(editItem => {
                editItem.Comment = "";
                editItem.SaveAllowed = true;
            });

            var nameItems = editItems.Select(editItem => new clsOntologyItem
            {
                Name = editItem.Name,
                GUID_Parent = type == localConfig.Globals.Type_Object ? editItem.GuidParent : null
            }).ToList();

            
            if (nameItems.Any())
            {
                    
                var oItems = elasticServiceAgent.ItemsExisting(nameItems, type);

                (from editItm in EditItemList_EditItems
                    from oItem in oItems
                    where editItm.Name.ToLower() == oItem.Name.ToLower() && editItm.Guid != oItem.GUID
                    select editItm).ToList().ForEach(editItm =>
                    {
                        if (!nameDoubleAllowed)
                        {
                            editItm.SaveAllowed = false;
                        }
                        else
                        {
                            editItm.Comment = "Existing";
                        }
                         
                    });

            }
          
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void TestParent(string idClass)
        {
            parentClass = elasticServiceAgent.GetOItem(idClass, localConfig.Globals.Type_Class);

            if (parentClass == null) return;

            Text_ParentText = parentClass.Name;

            IsChecked_ParentItem = false;
            ViewValidation();
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            
            
            if (message.ChannelId == Channels.SelectedClassNode)
            {
                var itemType = "";
                int selectedIndex;
                if (int.TryParse(Id_ItemType, out selectedIndex))
                {
                    itemType = StringList_DropDownItems[selectedIndex];
                }

                if (IsChecked_ParentItem  && itemType == localConfig.Globals.Type_Object)
                {

                    
                    IsEnabled_NewItem = false;
                    var selectedClass = message.OItems.First();

                    TestParent(selectedClass.GUID);

                }
                else if (IsChecked_Item && itemType == localConfig.Globals.Type_Class)
                {
                    IsEnabled_NewItem = false;
                    var selectedClass = message.OItems.First();

                    var existingItem = EditItemList_EditItems.FirstOrDefault(item => item.Guid == selectedClass.GUID);

                    if (existingItem != null) return;

                    var parentItem = elasticServiceAgent.GetOItem(selectedClass.GUID_Parent, localConfig.Globals.Type_Class);

                    if (parentItem == null) return;

                    existingItem = new EditItem
                    {
                        Guid = selectedClass.GUID,
                        Name = selectedClass.Name,
                        ItemType = localConfig.Globals.Type_Class,
                        GuidParent = parentItem.GUID,
                        NameParent = parentItem.Name,
                        Save = true
                    };


                    EditItemList_EditItems.Add(existingItem);
                    ViewValidation();
                }

                
            }
            else if (message.ChannelId == Channels.ParameterList && itemType == localConfig.Globals.Type_Object)
            {
                var itemType = "";
                int selectedIndex;
                if (int.TryParse(Id_ItemType, out selectedIndex))
                {
                    itemType = StringList_DropDownItems[selectedIndex];
                }

                if (IsChecked_Item)
                {
                    IsEnabled_NewItem = false;
                    var selectedObject = message.OItems.First();

                    SelectedObject(selectedObject);

                }

                
            }

            
        }

        private void SelectedObject(clsOntologyItem selectedObject)
        {
            var existingItem = EditItemList_EditItems.FirstOrDefault(item => item.Guid == selectedObject.GUID);

            if (existingItem != null) return;

            var parentItem = elasticServiceAgent.GetOItem(selectedObject.GUID_Parent, localConfig.Globals.Type_Class);

            if (parentItem == null) return;

            existingItem = new EditItem
            {
                Guid = selectedObject.GUID,
                Name = selectedObject.Name,
                ItemType = localConfig.Globals.Type_Object,
                GuidParent = parentItem.GUID,
                NameParent = parentItem.Name,
                Save = true
            };


            EditItemList_EditItems.Add(existingItem);

            EditItem_EditItemCreate = existingItem;
            ViewValidation();
        }

   
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
