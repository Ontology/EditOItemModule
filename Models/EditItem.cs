﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditOItemModule.Models
{
    public class EditItem
    {
        public bool Save { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string ItemType { get; set; }
        public string GuidParent { get; set; }
        public string NameParent { get; set; }
        public string Comment { get; set; }
        public bool SaveAllowed { get; set; }
    }
}
