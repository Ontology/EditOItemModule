﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditOItemModule.Models
{
    public class AttributeItem
    {
        public string IdAttributeType { get; set; }
        public string NameAttributeType { get; set; }

        public string IdDataType { get; set; }
        public string NameDataType { get; set; }

        public string IdObject { get; set; }
        public string NameObject { get; set; }

        public string IdClass { get; set; }
        public string NameClass { get; set; }

        public string IdAttribute { get; set; }

        public object AttributeValue { get; set; }

    }
}
