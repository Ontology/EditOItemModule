﻿using EditOItemModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditOItemModule.Services
{
    public class ElasticServiceAgent : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderOItem;
        private OntologyModDBConnector dbReaderExisting;
        private OntologyModDBConnector dbWriter;
        private OntologyModDBConnector dbReaderAttributeById;

        

        public ElasticServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);
            dbReaderExisting = new OntologyModDBConnector(localConfig.Globals);
            dbWriter = new OntologyModDBConnector(localConfig.Globals);
            dbReaderAttributeById = new OntologyModDBConnector(localConfig.Globals);

            
        }

        public clsOntologyItem GetOItem(string guidItem, string type)
        {
            return dbReaderOItem.GetOItem(guidItem, type);
        }

        public clsOntologyItem IsClassChild(clsOntologyItem classItem, string idClassToCheck)
        {
            var searchClasses = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = classItem.GUID
                }
            }.ToList();

            var result = dbReaderOItem.GetDataClasses(searchClasses);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (dbReaderOItem.Classes1.Any(cls => cls.GUID == idClassToCheck))
            {
                return localConfig.Globals.LState_Success.Clone();
            }

            var classList = dbReaderOItem.Classes1.Select(clsItem => clsItem.Clone()).ToList();

            foreach(var clsItem in classList)
            {
                result = IsClassChild(clsItem, idClassToCheck);

                if (result.GUID == localConfig.Globals.LState_Success.GUID || result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            return localConfig.Globals.LState_Nothing.Clone();
        }

        public clsOntologyItem SaveEditItems(List<EditItem> editItems)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var attributeTypes = editItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_AttributeType).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                GUID_Parent = editItem.GuidParent,
                Type = localConfig.Globals.Type_AttributeType
            }).ToList();

            if (attributeTypes.Any())
            {
                result = dbWriter.SaveAttributeTypes(attributeTypes);
            }

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            var relationTypes = editItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_RelationType).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                Type = localConfig.Globals.Type_RelationType
            }).ToList();

            if (relationTypes.Any())
            {
                result = dbWriter.SaveRelationTypes(relationTypes);
            }

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            var classes = editItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_Class).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                GUID_Parent = editItem.GuidParent,
                Type = localConfig.Globals.Type_Class
            }).ToList();

            if (classes.Any())
            {
                result = dbWriter.SaveClass(classes);
            }

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            var objects = editItems.Where(editItem => editItem.ItemType == localConfig.Globals.Type_Object).Select(editItem => new clsOntologyItem
            {
                GUID = editItem.Guid,
                Name = editItem.Name,
                GUID_Parent = editItem.GuidParent,
                Type = localConfig.Globals.Type_Object
            }).ToList();

            if (objects.Any())
            {
                result = dbWriter.SaveObjects(objects);
            }

            return result;
        }

        public List<clsOntologyItem> ItemsExisting(List<clsOntologyItem> searchItems, string itemType)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (itemType == localConfig.Globals.Type_AttributeType)
            {
                result = dbReaderExisting.GetDataAttributeType(searchItems);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return null;
                }
                return dbReaderExisting.AttributeTypes;
            }
            else if (itemType == localConfig.Globals.Type_RelationType)
            {
                result = dbReaderExisting.GetDataRelationTypes(searchItems);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return null;
                }
                return dbReaderExisting.RelationTypes;
            }
            else if (itemType == localConfig.Globals.Type_Object)
            {
                result = dbReaderExisting.GetDataObjects(searchItems);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return null;
                }
                return dbReaderExisting.Objects1;
            }
            else if (itemType == localConfig.Globals.Type_Class)
            {
                result = dbReaderExisting.GetDataClasses(searchItems);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return null;
                }
                return dbReaderExisting.Classes1;
            }
            else
            {
                return null;
            }

            

            
        }

        public clsObjectAtt GetObjectAttribute(string idAttribute)
        {
            var searchAttribute = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_Attribute = idAttribute
                }
            };

            var result = dbReaderAttributeById.GetDataObjectAtt(searchAttribute);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            return dbReaderAttributeById.ObjAtts.FirstOrDefault();
        }

        

        public void StopRead()
        {

        }
    }
}
