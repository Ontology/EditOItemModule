﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EditOItemModule.Translations
{
    public class TranslationController
    {

        
        public string Title_FileStreamError
        {
            get
            {
                return GetValue("244d7e5cd2e64401bd99a91ba98fda34", "Datei-Fehler!");
            }

        }

        public string Text_FileStreamError
        {
            get
            {
                return GetValue("5682b2ddf0be4d1babeac59245d25a85", "Datei für Datenaustausch konnte  nicht agespeichert werden!");
            }

        }

        public string Title_WebConfigError
        {
            get
            {
                return GetValue("78634024801646ceba093bb8c62f7f0a", "Webserver-Fehler");
            }

        }

        public string Text_WebConfigError
        {
            get
            {
                return GetValue("7d1199b177ba435b8c2bcb917cec913b", "Der Webserver hat einen Fehler verursacht!");
            }

        }

        public string Text_MenuEntryEdit
        {
            get
            {
                return GetValue("Text_MenuEntryEdit", "Edit");
            }
        }

        public string Text_MenuEntryList
        {
            get
            {
                return GetValue("Text_MenuEntryList", "List");
            }
        }

        public string Text_MenuEntryPassword
        {
            get
            {
                return GetValue("Text_MenuEntryPassword", "Password");
            }
        }

        public string Label_Guid
        {
            get
            {
                return GetValue("Label_Guid", "Guid:");
            }
        }

        public string Label_Name
        {
            get
            {
                return GetValue("Label_Name", "Name:");
            }
        }

        public string Label_Save
        {
            get
            {
                return GetValue("Label_Save", "Save");
            }
        }

        public string Label_Apply
        {
            get
            {
                return GetValue("Label_Apply", "Apply");
            }
        }

        public string Label_Cancel
        {
            get
            {
                return GetValue("Label_Cancel", "Cancel");
            }
        }

        public string Label_ItemType
        {
            get
            {
                return GetValue("Label_ItemType", "Itemtype:");
            }
        }

        public string Label_Parent
        {
            get
            {
                return GetValue("Label_Parent", "Parent:");
            }
        }

        public string Label_NewGuid
        {
            get
            {
                return GetValue("Label_NewGuid", "New Guid");
            }
        }

        public string Col_Save
        {
            get
            {
                return GetValue("Col_Save", "Save");
            }
        }

        public string Col_Guid
        {
            get
            {
                return GetValue("Col_Guid", "Id");
            }
        }

        public string Col_Name
        {
            get
            {
                return GetValue("Col_Name", "Name");
            }
        }

        public string Col_ItemType
        {
            get
            {
                return GetValue("Col_ItemType", "Item-Type");
            }
        }

        public string Col_NameParent
        {
            get
            {
                return GetValue("Col_NameParent", "Parent");
            }
        }

        public string Col_Comment
        {
            get
            {
                return GetValue("Col_Comment", "Note");
            }
        }

        public string Label_Listen
        {
            get
            {
                return GetValue("Label_Listen", "Listen");
            }
        }

        public string Label_ListenParent
        {
            get
            {
                return GetValue("Label_ListenParent", "Listen Parent");
            }
        }

        public string Label_NewItem
        {
            get
            {
                return GetValue("Label_NewItem", "New Item");
            }
        }

        public string Label_DeleteItem
        {
            get
            {
                return GetValue("Label_DeleteItem", "Delete Item");
            }
        }

        public string Label_ItemIdentification
        {
            get
            {
                return GetValue("Label_ItemIdentification", "Item:");
            }
        }

        public string Label_ExitMessage
        {
            get
            {
                return GetValue("ExitMessage", "Wollen Sie die Seite wirklich schließen?");
            }
        }

        public string GetTranslatedByPropertyName(string propertyName)
        {
            var property = this.GetType().GetProperties().Cast<PropertyInfo>().FirstOrDefault(propItem => propItem.Name == propertyName);

            if (property != null)
            {
                var value = property.GetValue(this);
                return value != null ? value.ToString() : "xxx_Error_xxx";
            }
            else
            {
                return "xxx_Error_xxx";
            }
        }


        private string GetValue(string IdReference, string defaultValue)
        {
            return defaultValue;
        }

    }
}
